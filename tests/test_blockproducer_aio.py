# -*- coding: utf-8 -*-
import aiounittest
from datetime import datetime
from .fixtures_aio import fixture_data, Blockproducer, Blockproducers, Account
from graphenecommon import exceptions


class Testcases(aiounittest.AsyncTestCase):
    def setUp(self):
        fixture_data()

    async def test_Blockproducer(self):
        w = await Blockproducer("1.6.1")
        account = await w.account
        self.assertIsInstance(account, Account)
        self.assertEqual(account["id"], "1.2.101")
        await Blockproducer(w)

    """
    def test_nonexist(self):
        with self.assertRaises(exceptions.AccountDoesNotExistsException):
            Blockproducer("foobar")

    def test_Blockproducers(self):
        ws = Blockproducers()
        self.assertEqual(len(ws), 2)

    def test_Blockproducers2(self):
        ws = Blockproducers("init0")
        self.assertEqual(len(ws), 1)
    """
