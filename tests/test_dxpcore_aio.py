# -*- coding: utf-8 -*-
import aiounittest
from graphenecommon import exceptions
from .fixtures_aio import fixture_data, Account, Dxpcore


class Testcases(aiounittest.AsyncTestCase):
    def setUp(self):
        fixture_data()

    async def test_Dxpcore(self):
        with self.assertRaises(exceptions.AccountDoesNotExistsException):
            await Dxpcore("FOObarNonExisting")

        c = await Dxpcore("1.5.0")
        self.assertEqual(c["id"], "1.5.0")
        self.assertIsInstance(await c.account, Account)

        with self.assertRaises(exceptions.DxpcoreMemberDoesNotExistsException):
            await Dxpcore("1.5.1")
