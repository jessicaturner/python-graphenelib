# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from .fixtures import fixture_data, Benefactor, Benefactors, Account
from graphenecommon import exceptions


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_benefactor(self):
        w = Benefactor("1.14.139")
        self.assertIsInstance(w["work_end_date"], datetime)
        self.assertIsInstance(w["work_begin_date"], datetime)
        self.assertIsInstance(w["work_begin_date"], datetime)
        self.assertIsInstance(w["daily_pay"], int)
        self.assertIsInstance(w.account, Account)
        self.assertEqual(w.account["id"], "1.2.100")
        Benefactor(w)

    def test_nonexist(self):
        with self.assertRaises(exceptions.BenefactorDoesNotExistsException):
            Benefactor("foobar")

    def test_benefactors(self):
        ws = Benefactors()
        self.assertEqual(len(ws), 2)

    def test_benefactors2(self):
        ws = Benefactors._cache["Benefactors"] = [dict(), dict()]
        self.assertEqual(len(ws), 2)
