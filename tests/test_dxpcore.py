# -*- coding: utf-8 -*-
import unittest
from graphenecommon import exceptions
from .fixtures import fixture_data, Account, Dxpcore


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_Dxpcore(self):
        with self.assertRaises(exceptions.AccountDoesNotExistsException):
            Dxpcore("FOObarNonExisting")

        c = Dxpcore("1.5.0")
        self.assertEqual(c["id"], "1.5.0")
        self.assertIsInstance(c.account, Account)

        with self.assertRaises(exceptions.DxpcoreMemberDoesNotExistsException):
            Dxpcore("1.5.1")
