# -*- coding: utf-8 -*-
import aiounittest
from datetime import datetime
from .fixtures_aio import fixture_data, Benefactor, Benefactors, Account
from graphenecommon import exceptions


class Testcases(aiounittest.AsyncTestCase):
    def setUp(self):
        fixture_data()

    async def test_benefactor(self):
        w = await Benefactor("1.14.139")
        self.assertIsInstance(w["work_end_date"], datetime)
        self.assertIsInstance(w["work_begin_date"], datetime)
        self.assertIsInstance(w["work_begin_date"], datetime)
        self.assertIsInstance(w["daily_pay"], int)
        account = await w.account
        self.assertIsInstance(account, Account)
        self.assertEqual(account["id"], "1.2.100")
        await Benefactor(w)

    async def test_nonexist(self):
        with self.assertRaises(exceptions.BenefactorDoesNotExistsException):
            await Benefactor("foobar")

    async def test_benefactors(self):
        ws = await Benefactors()
        self.assertEqual(len(ws), 2)
