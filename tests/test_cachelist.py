# -*- coding: utf-8 -*-
import unittest
from .fixtures import fixture_data, Blockproducers


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_blockproducers(self):
        Blockproducers._cache["Blockproducers"] = [dict(id="1.6.0", blockproducer_account="init0")]
        w = Blockproducers()
        self.assertIn("1.6.0", w)
        Blockproducers.clear_cache()
        self.assertFalse(Blockproducers())
