# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from .fixtures import fixture_data, Blockproducer, Blockproducers, Account
from graphenecommon import exceptions


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_Blockproducer(self):
        w = Blockproducer("1.6.1")
        self.assertIsInstance(w.account, Account)
        self.assertEqual(w.account["id"], "1.2.101")
        Blockproducer(w)

    """
    def test_nonexist(self):
        with self.assertRaises(exceptions.AccountDoesNotExistsException):
            Blockproducer("foobar")

    def test_Blockproducers(self):
        ws = Blockproducers()
        self.assertEqual(len(ws), 2)

    def test_Blockproducers2(self):
        ws = Blockproducers("init0")
        self.assertEqual(len(ws), 1)
    """
