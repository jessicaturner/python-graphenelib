# -*- coding: utf-8 -*-
from ..exceptions import BenefactorDoesNotExistsException
from ..utils import formatTimeString
from .blockchainobject import BlockchainObject, BlockchainObjects
from ..benefactor import Benefactor as SyncBenefactor, Benefactors as SyncBenefactors


class Benefactor(BlockchainObject, SyncBenefactor):
    """ Read data about a benefactor in the chain

        :param str id: id of the benefactor
        :param instance blockchain_instance: instance to use when accesing a RPC

    """

    async def __init__(self, *args, **kwargs):
        self.define_classes()
        assert self.account_class
        assert self.type_id
        await BlockchainObject.__init__(self, *args, **kwargs)
        self.post_format()

    async def refresh(self):
        benefactor = await self.blockchain.rpc.get_object(self.identifier)
        if not benefactor:
            raise BenefactorDoesNotExistsException
        await super(Benefactor, self).__init__(benefactor, blockchain_instance=self.blockchain)
        self.post_format()

    @property
    async def account(self):
        return await self.account_class(
            self["benefactor_account"], blockchain_instance=self.blockchain
        )


class Benefactors(BlockchainObjects, SyncBenefactors):
    """ Obtain a list of benefactors for an account

        :param str account_name/id: Name/id of the account (optional)
        :param instance blockchain_instance: instance to use when accesing a RPC
    """

    async def __init__(self, *args, account_name=None, lazy=False, **kwargs):
        self.define_classes()
        assert self.account_class
        assert self.benefactor_class

        self.account_name = account_name
        self.lazy = lazy
        await super().__init__(*args, **kwargs)

    async def refresh(self, *args, **kwargs):
        if self.account_name:
            account = await self.account_class(
                self.account_name, blockchain_instance=self.blockchain
            )
            self.benefactors = await self.blockchain.rpc.get_benefactors_by_account(
                account["id"]
            )
        else:
            self.benefactors = await self.blockchain.rpc.get_all_benefactors()

        self.store(
            [
                await self.benefactor_class(
                    x, lazy=self.lazy, blockchain_instance=self.blockchain
                )
                for x in self.benefactors
            ]
        )
